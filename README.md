Lambda Core
===========
OpenGL32 hooking library for GoldSource-based engine games.

[![showcase][showcase]][homepage]

Features
--------
- XQZ Wallhack
- Lambert

Installation
------------
```
$ powershell -Command Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
$ powershell -File build.ps1
```

Distribution
------------
This project is licensed under the terms of the [MIT License](LICENSE).

[homepage]: <https://gitlab.com/amalchuk/lambda_core>
[showcase]: <showcase.png>
